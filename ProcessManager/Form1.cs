using Microsoft.VisualBasic.Logging;
using System;
using System.Diagnostics;

namespace ProcessManager
{
    public partial class Form1 : Form
    {
       
        public Form1()
        {
            InitializeComponent();
            dataGridView1.AllowUserToAddRows = false;
            comboBox1.SelectedIndex = 1;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
        private void LoadData(Process myprocess)
        {
            string[] row = { myprocess.Id.ToString(), myprocess.ProcessName, (myprocess.PagedMemorySize64 / 1000000.0).ToString() + " MB", myprocess.StartTime.ToString(), myprocess.TotalProcessorTime.ToString(), myprocess.PriorityClass.ToString(), myprocess.Threads.Count.ToString() };
            dataGridView1.Rows.Add(row);
        }

        private void listBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if(listBox.SelectedIndex == 0)
            {
                string path = "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe";
                var proc = Process.Start(path);
                LoadData(proc);
            }
            else if (listBox.SelectedIndex == 1)
            {
                string path = "D:\\Telegram Desktop\\Telegram.exe";
                var proc = Process.Start(path);
                LoadData(proc);
            }
            else if (listBox.SelectedIndex == 2)
            {
                string path = "D:\\Epic Games\\Launcher\\Portal\\Binaries\\Win32\\EpicGamesLauncher.exe";
                var proc = Process.Start(path);
                LoadData(proc);
            }
            else if (listBox.SelectedIndex == 3)
            {
                string path = "D:\\Microsoft VS Code\\Code.exe";
                var proc = Process.Start(path);
                LoadData(proc);
            }
            else if (listBox.SelectedIndex == 4)
            {
                string path = "C:\\Users\\����\\AppData\\Roaming\\Spotify\\Spotify.exe";
                var proc = Process.Start(path);
                LoadData(proc);
            }
        }

        private void ExitProcess_Click(object sender, EventArgs e)
        {
            int index = dataGridView1.CurrentRow.Index;
            string name = dataGridView1[1, index].Value.ToString();
            var _process = Process.GetProcessesByName(name);
            try
            {
                foreach (Process p in _process)
                {
                    p.Kill();
                }
                dataGridView1.Rows.RemoveAt(index);
                dataGridView1.Refresh();
                MessageBox.Show("������ ���������!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Prioritys_Click(object sender, EventArgs e)
        {
            int index = dataGridView1.CurrentRow.Index;
            string name = dataGridView1[1, index].Value.ToString();
            var _process = Process.GetProcessesByName(name);
            try
            {
                foreach (Process p in _process)
                {
                    
                        if (comboBox1.SelectedIndex == 0) p.PriorityClass = ProcessPriorityClass.Idle;
                        if (comboBox1.SelectedIndex == 1) p.PriorityClass = ProcessPriorityClass.Normal;
                        if (comboBox1.SelectedIndex == 2) p.PriorityClass = ProcessPriorityClass.High;
                        if (comboBox1.SelectedIndex == 3) p.PriorityClass = ProcessPriorityClass.RealTime;
                        dataGridView1[5, index].Value = p.PriorityClass.ToString();
                   
                }
                dataGridView1.Refresh();
                MessageBox.Show("������� ������!");
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }   
        }
    }
}