﻿namespace EncodyngFileApp
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SelectFile = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.FileName = new System.Windows.Forms.Label();
            this.Key = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.progBar = new System.Windows.Forms.ProgressBar();
            this.Ciper = new System.Windows.Forms.Button();
            this.backWork = new System.ComponentModel.BackgroundWorker();
            this.Deciper = new System.Windows.Forms.Button();
            this.backWorkTwo = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // SelectFile
            // 
            this.SelectFile.BackColor = System.Drawing.Color.Transparent;
            this.SelectFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.SelectFile.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SelectFile.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.SelectFile.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.SelectFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SelectFile.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.SelectFile.Location = new System.Drawing.Point(179, 56);
            this.SelectFile.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SelectFile.Name = "SelectFile";
            this.SelectFile.Size = new System.Drawing.Size(103, 30);
            this.SelectFile.TabIndex = 0;
            this.SelectFile.Text = "Вибрати файл";
            this.SelectFile.UseVisualStyleBackColor = false;
            this.SelectFile.Click += new System.EventHandler(this.SelectFile_Click);
            // 
            // FileName
            // 
            this.FileName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.FileName.Font = new System.Drawing.Font("Segoe UI", 11.78182F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FileName.Location = new System.Drawing.Point(96, 97);
            this.FileName.Name = "FileName";
            this.FileName.Size = new System.Drawing.Size(275, 22);
            this.FileName.TabIndex = 1;
            // 
            // Key
            // 
            this.Key.Location = new System.Drawing.Point(96, 165);
            this.Key.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Key.Name = "Key";
            this.Key.Size = new System.Drawing.Size(275, 23);
            this.Key.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 11.78182F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(179, 138);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Введіть ключ";
            // 
            // progBar
            // 
            this.progBar.Location = new System.Drawing.Point(96, 313);
            this.progBar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.progBar.Name = "progBar";
            this.progBar.Size = new System.Drawing.Size(275, 33);
            this.progBar.TabIndex = 4;
            // 
            // Ciper
            // 
            this.Ciper.Font = new System.Drawing.Font("Segoe UI", 13.74545F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Ciper.Location = new System.Drawing.Point(96, 221);
            this.Ciper.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Ciper.Name = "Ciper";
            this.Ciper.Size = new System.Drawing.Size(275, 46);
            this.Ciper.TabIndex = 6;
            this.Ciper.Text = "Зашифрувати";
            this.Ciper.UseVisualStyleBackColor = true;
            this.Ciper.Click += new System.EventHandler(this.Ciper_Click);
            // 
            // backWork
            // 
            this.backWork.WorkerReportsProgress = true;
            this.backWork.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backWork_DoWork);
            this.backWork.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backWork_ProgressChanged);
            this.backWork.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backWork_RunWorkerCompleted);
            // 
            // Deciper
            // 
            this.Deciper.Font = new System.Drawing.Font("Segoe UI", 13.74545F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Deciper.Location = new System.Drawing.Point(96, 271);
            this.Deciper.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Deciper.Name = "Deciper";
            this.Deciper.Size = new System.Drawing.Size(275, 38);
            this.Deciper.TabIndex = 6;
            this.Deciper.Text = "Розшифрувати";
            this.Deciper.UseVisualStyleBackColor = true;
            this.Deciper.Click += new System.EventHandler(this.Deciper_Click);
            // 
            // backWorkTwo
            // 
            this.backWorkTwo.WorkerReportsProgress = true;
            this.backWorkTwo.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backWorkTwo_DoWork);
            this.backWorkTwo.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backWorkTwo_ProgressChanged);
            this.backWorkTwo.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backWorkTwo_RunWorkerCompleted);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(485, 427);
            this.Controls.Add(this.Deciper);
            this.Controls.Add(this.Ciper);
            this.Controls.Add(this.progBar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Key);
            this.Controls.Add(this.FileName);
            this.Controls.Add(this.SelectFile);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private OpenFileDialog openFileDialog1;
        private Button SelectFile;
        private SaveFileDialog saveFileDialog1;
        private Label FileName;
        private TextBox Key;
        private Label label1;
        private ProgressBar progBar;
        private Button Ciper;
        private System.ComponentModel.BackgroundWorker backWork;
        private Button Deciper;
        private System.ComponentModel.BackgroundWorker backWorkTwo;
    }
}