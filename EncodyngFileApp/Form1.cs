using System.ComponentModel;
using System;
using System.IO;
using System.Net.NetworkInformation;
using System.Threading;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;
using System.Diagnostics;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace EncodyngFileApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
           
        }

        private void SelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            FileName.Text = openFileDialog1.SafeFileName;
        }

        private void Ciper_Click(object sender, EventArgs e)
        {
            if (backWork.IsBusy != true)
            {
                backWork.RunWorkerAsync();
            }

        }

        private void backWork_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            progBar.Value = e.ProgressPercentage;
        }

        private void backWork_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
           
            Cipers(sender, ".cripted");
          
        }

        private void backWork_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show("Error: " + e.Error.Message);
            }
            else
            {
                MessageBox.Show("Done!");
                progBar.Value = 100;
            }
        }

        private void Deciper_Click(object sender, EventArgs e)
        {
            if (backWorkTwo.IsBusy != true)
            {
                // Start the asynchronous operation.
                backWorkTwo.RunWorkerAsync();
            }
        }

        private void backWorkTwo_DoWork(object sender, DoWorkEventArgs e)
        {
            Cipers(sender,".decripted");
        }

        private void backWorkTwo_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progBar.Value = e.ProgressPercentage;
        }

        private void backWorkTwo_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            if (e.Error != null)
            {
                MessageBox.Show("�������: " + e.Error.Message);
            }
            else
            {
                MessageBox.Show("��������!");
                progBar.Value = 100;
            }
        }

        private void Cipers(object Sender,string roz)
        {
            Stopwatch stopwatch = new Stopwatch();
            BackgroundWorker? worker = Sender as BackgroundWorker;
            string filePath = openFileDialog1.FileName;
            string key = Key.Text;
            using (FileStream fileStream = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                using (StreamWriter streamWriter = new StreamWriter(filePath + roz))
                {
                    using (StreamReader streamReader = new StreamReader(fileStream))
                    {
                        if (File.Exists(filePath))
                        {
                            try
                            {
                                stopwatch.Start();
                                string fileText = streamReader.ReadToEnd();
                                var r = key;
                                while (r.Length < fileText.Length)
                                {
                                    r += r;
                                }

                                var currentKey = r.Substring(0, fileText.Length);
                                var res = string.Empty;
                                int t = 1;
                                int seconds = 1000;
                                var Oneprosent = fileText.Length / 100;
                                int prosent = 0;
                                for (var i = 0; i < fileText.Length; i++)
                                {
                                    res += ((char)(fileText[i] ^ currentKey[i])).ToString();
                                    if (i == (prosent * fileText.Length) / 100)
                                    {
                                        worker.ReportProgress(prosent);
                                        ++prosent;

                                    }
                                    if (stopwatch.ElapsedMilliseconds == seconds)
                                    {
                                        seconds += 1000;
                                    }

                                }
                                streamWriter.WriteLine(res);
                                long length = new System.IO.FileInfo(filePath).Length;
                                stopwatch.Stop();
                                MessageBox.Show("���������� ���������\n��'� �����: " + openFileDialog1.SafeFileName + "\n����� �����: " + length + "\n��� ����������: " + stopwatch.ElapsedMilliseconds + "��");

                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show($"{ex}\n��������� �� ���");
                                File.Delete(filePath + ".crypted");
                            }

                        }
                        else
                        {
                            MessageBox.Show("������ ����� �� ����");
                        }
                    }
                }

            }
        }
    }
}